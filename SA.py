from SALib.sample import saltelli
from SALib.analyze import sobol
from SALib.test_functions import Ishigami

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Define the model inputs
problem = {
    'num_vars': 3,
    'names': ['x1', 'x2', 'x3'],
    'bounds': [[-3.14159265359, 3.14159265359],
               [-3.14159265359, 3.14159265359],
               [-3.14159265359, 3.14159265359]]
}

# Generate samples
param_values = saltelli.sample(problem, 100, calc_second_order=True)
print('Input samples: ({})'.format(len(param_values)))
print(param_values)

# Run model (example)
Y = Ishigami.evaluate(param_values)

# Compute characteristics
m = []
a = []
for sample in param_values.tolist():
    m.append((sample[0] + sample[1] + sample[2]) / 3)  # You can compute characteristic time here
    a.append(abs(sample[0] + sample[1] + sample[2]) / 3)  # You can compute mesh volume here

# Perform analysis
Si = sobol.analyze(problem, Y, print_to_console=False)

# Print the 1st order sensitivity indices
print('1st order sensitivity indices:', Si['S1'])

# Print 2nd order sensitivity indices
print("x1-x2:", Si['S2'][0, 1])
print("x1-x3:", Si['S2'][0, 2])
print("x2-x3:", Si['S2'][1, 2])

ax.scatter(m, a, Y)

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()

