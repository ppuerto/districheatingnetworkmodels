from SALib.sample import saltelli
from SALib.analyze import sobol
from model import evaluate_model
from model import evaluate_model_A
from model import evaluate_model_B
import numpy as np
from pylab import *

from mpl_toolkits.mplot3d import Axes3D


# Define the model inputs
problem = {
    'num_vars': 4,
    'names': ['l', 'd', 'N', 'mp'],
    'bounds': [[50, 150], # [m]
               [0.15, 0.75], # [m]
               [10, 100], # mesh
               [0.025, 0.075]] # [m3/s]
}

# Generate samples
print('Generating samples ...')
param_values = saltelli.sample(problem, 50, calc_second_order=True)
print('Input samples: ({})'.format(len(param_values)))
print(param_values)

# Run model (example)
# PAs possible ici Y = evaluate_model.evaluate(param_values) # ici j'ai remplacé Ishigami par evaluate_model

print('Running model...')
Y = np.empty([param_values.shape[0]])# creation list initialisation = list of zero

for i, X in enumerate(param_values):
    Y[i] = evaluate_model(X)   # vector of T_out

print(Y)



Y_A = np.empty([param_values.shape[0]])

for i, X in enumerate(param_values):
    Y_A[i] = evaluate_model_A(X)   # vector of Tau

print(Y_A)

Y_B = np.empty([param_values.shape[0]])

for i, X in enumerate(param_values):
    Y_B[i] = evaluate_model_B(X)   # vector of delta

print(Y_B)

# Graph T_out = fct(Tau)

plot(Y_A, Y)
xlabel("Tau")
ylabel("T_out")
title("T_out = fct(Tau)")

show()

# Graph T_out = fct(delta)

plot(Y_B, Y)
xlabel("delta")
ylabel("T_out")
title("T_out = fct(delta)")

show()

"""# Plot 3D  (cette partie du code fonctionne mais avec un nombre de samples élevé, ça ram un peu)

fig = figure()
ax = Axes3D(fig)
Y_A, Y_B = np.meshgrid(Y_A, Y_B)

ax.plot_surface(Y_A, Y_B, Y)


ax.set_xlabel('Tau')
ax.set_ylabel('delta')
ax.set_zlabel('T_out')

title("T_out = fct(Tau, delta)")

show()"""

print('Performing analysis...')
# Perform analysis
Si = sobol.analyze(problem, Y, print_to_console=False)

# Print the 1st order sensitivity indices
print('1st order sensitivity indices:', Si['S1'])

# Print 2nd order sensitivity indices


#print("x1-x2:", Si['S2'][0, 1]) # pas besoin car pas d'analyse entre nos variables
#print("x1-x3:", Si['S2'][0, 2])
#print("x2-x3:", Si['S2'][1, 2])
