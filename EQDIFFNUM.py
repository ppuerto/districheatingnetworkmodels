from numpy import linspace, pi, exp, log10
from scipy.integrate import odeint



#Fonction run_model : Calculation of temperatures throughout the tube at each timestep using the differential equation system

T_0=50
T_in=80
T_ext=10
rho=990
cp=4184
h=1.5
time= 10 * 60

Vp=0.05
D=0.3
L=100
N=30
nu = 5.8e-7
mu = rho * nu


V_m = pi * D ** 2 / 4 * L / N
A_lm = pi * D * L / N
A = pi * D ** 2 / 4

Re = (Vp/A)*D/nu

if Re <= 2300:
    f=64/Re
elif Re > 2300:
    f=1/(1.82*log10(Re) - 1.64)**2

print(Re)
print(f)

def run_model():

    def eq_diff(y, t):
        dydt = []
        for idx, t in enumerate([T_0] * N):
            if idx == 0:
                T_input = T_in
            else:
                T_input = y[idx - 1]

            eq = (T_input - y[idx]) * Vp * rho * cp  # exchange between meshes
            eq -= (y[idx] - T_ext) * h * A_lm # loss with environment
            eq -= (Vp * f * (L/(2*D)) * mu * (Vp/A)**2) # loss friction
            eq /= V_m * rho * cp
            dydt.append(eq)

        return dydt



    temps = linspace(0, time, num=(time / 15)) # (arg['step'] / 15) values between 0 and 'step'
    y0 = [T_0] * N
    sol = odeint(eq_diff, y0, temps).tolist()

    T_out=sol[-1]
    #print (sol[-1])
    return T_out #the last timestep




def solution():
    a=(Vp*rho*cp + h*A_lm)/(rho*V_m*cp)
    results =[]

    T_input = T_in
    for i in range(0, N):
        b = (Vp*rho*cp*T_input + h*A_lm*T_ext - (Vp * f * (L/(2*D)) * mu * (Vp/A)**2))/(rho*V_m*cp)
        y = (b/a)*exp(a * time)*exp(-a * time) + T_0*exp(-a * time)
        results.append(y)
        T_input = y

    return results

print(solution())
print (run_model())


def error(results, T_out):
    err = []
    for i in range(0,N):
        err.append(results[i] - T_out[i])

    return err

print(error(solution(),run_model()))


