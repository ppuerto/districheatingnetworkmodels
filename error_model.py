from numpy import linspace, pi, log10
from scipy.integrate import odeint

#Fonction run_model : Calculation of temperatures throughout the tube at each timestep using the differential equation system
def run_model(arg):
    mesh_volume = pi * arg['diameter'] ** 2 / 4 * arg['length'] / arg['nbr_mesh']
    mesh_loss_area = pi * arg['diameter'] * arg['length'] / arg['nbr_mesh']
    A = pi * arg['diameter'] ** 2 / 4

    Re = (arg['flow_in']/A)*arg['diameter']/arg['nu']
    print(Re)
    if Re <= 2300:
        f=64/Re
    elif Re > 2300:
        f=1/(1.82*log10(Re) - 1.64)**2


    def eq_diff(y, t):
        dydt = []
        for idx, t in enumerate([arg['t_0']] * arg['nbr_mesh']):
            if idx == 0:
                t_input = arg['t_in']
            else:
                t_input = y[idx - 1]

            eq = (t_input - y[idx]) * arg['flow_in'] * arg['rho'] * arg['cp']  # exchange between meshes
            eq -= (y[idx] - arg['t_ext']) * arg['u_tot'] * mesh_loss_area  # loss with environment
            eq /= mesh_volume * arg['rho'] * arg['cp']
            eq -= (arg['flow_in'] * f * (arg['length']/(2*arg['diameter'])) * arg['mu'] * (arg['flow_in']/A)**2) # loss friction
            dydt.append(eq)


        return dydt



    time = linspace(0, arg['step'], num=(arg['step'] / 15)) # (arg['step'] / 15) values between 0 and 'step'
    y0 = [arg['t_0']] * arg['nbr_mesh']
    sol = odeint(eq_diff, y0, time).tolist()

    print (sol[-1])

    return sol[-1] #the last timestep

###########################################################################################################

#Function difference : evaluate error between our case and the reference case while doing the quotient of temperatures. Return the average of quotients of temperatures
def difference(l_res, l_ref):

    diff = [] # an empty list
    for i in range(len(l_res)):
        diff.append(l_res[i] / l_ref[i])

    average = sum(diff)/len(diff)

    #todo

    return average

###########################################################################################################

#Function evaluate_error: creation of list variable res and list fix ref. Return function creation.
def evaluate_error(x):
    arg_res = {'nbr_mesh': x[0], 'step': 10 * 60, 'diameter': x[1], 'length': x[2],
               't_0': 50, 't_in': 80, 'flow_in': x[3],
               'rho': 990, 'cp': 4184, 't_ext': 10, 'u_tot': 1.5, 'nu': 5.8e-7, 'mu':990 * 5.8e-7 }


    res = run_model(arg_res) # return list of T variable for time step given by sol[] in the function run_model

    arg_ref = {'nbr_mesh': 150, 'step': 10 * 60, 'diameter': x[1], 'length': x[2],
               't_0': 50, 't_in': 80, 'flow_in': x[3],
               'rho': 990, 'cp': 4184, 't_ext': 10, 'u_tot': 1.5, 'nu': 5.8e-7, 'mu':990 * 5.8e-7 }

    ref = run_model(arg_ref) # return list of T fix(150) for time step given by sol[] in the function run_model

    return creation(res,ref)

###########################################################################################################

#Function creation: creation of 2 new lists of same size (equal to len(res)*len(ref)= variable at each generation of samples)
def creation(x,y):

    newlist1 = []
    newlist2 = []

    A = len(x)
    B = A - 1
    C = len(y)
    D = C - 1

    for j in range(0, B):
            w = linspace(x[j], x[j+1], D).tolist()
            for value in w:
                newlist1.append(value)

    for k in range(0, D):
            u = linspace(y[k], y[k+1], B).tolist()
            for value in u:
                newlist2.append(value)


    return difference(newlist1,newlist2)


###########################################################################################################

if __name__ == '__main__':
    e = evaluate_error([20, 0.5, 100, 0.01])
    print('error ->', e)
