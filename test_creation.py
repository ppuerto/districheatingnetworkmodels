from numpy import linspace

# Code d'essai comparaison vecteurs tailles differentes pour nombre de mesh(15) et nombre de samples(6) inférieur


res_1 = [80, 75, 60]
res_2 = [80, 70, 65, 62]
res_3 = [80, 76, 72, 68, 64, 62]
res_4 = [80, 75, 70, 65, 60]
res_5 = [80, 78, 73, 69, 66, 63, 60]
res_6 = [80, 60]

res = [80, 70, 65]
ref = [80, 78.5, 77, 75.5, 74, 72.5, 71, 69.5, 68, 66.5, 65, 63.5, 61, 59.5, 58]


def difference(l_res, l_ref):
    diff = l_res[-1] / l_ref[-1]
    print(diff)
    return diff


###########################################################################################################

def evaluate_error(x):

    #res = [80, 70, 65, 62]
    #ref = [80, 78.5, 77, 75.5, 74, 72.5, 71, 69.5, 68, 66.5, 65, 63.5, 61, 59.5, 58]

    return creation(res, ref)


def creation(x,y):
    newlist1 = []
    newlist2 = []

    A = len(x)
    B = A - 1
    C = len(y)
    D = C - 1


#    if A == 4 or A == 2 or A == 6 : # si la taille de res est un multiple de 15
#        for i in range(0, B) : # i : elmt 0, elmt 1,... etc...

#            if A == 4 : # ici il va y avoir 3 remplissages : de T1(x[0]) à T2(x[1]) , de T2(x[1]) à T3(x[2] et de T3(x[2]) à T4(x[3])
#                z = linspace(x[i], x[i+1], 5)
#                for value in z:
#                    newlist1.append(value)
#                newlist1.append(z)
#            elif A == 2 :
#                z = linspace(x[i], x[i+1], 15)
#                newlist1.append(z)
#            else :# ie len(x) == 5
#                z = linspace(x[i], x[i+1], 3)
#                newlist1.append(z)

    #else:

    for j in range(0, B):
            w = linspace(x[j], x[j+1], D).tolist()
            print("w...")
            print(w)
            for value in w:
                newlist1.append(value)
                #newlist1.append(w)

    for k in range(0, D):
            u = linspace(y[k], y[k+1], B).tolist()
            for value in u:
                newlist2.append(value)
                #newlist2.append(u)


    print("newlist1..")
    print(newlist1)

    print("newlist2..")
    print(newlist2)

    N2 = len(newlist1)
    print(N2)
    N3 = len(newlist2)
    print(N3)

    return difference(newlist1,newlist2)





print (creation(res,ref))

#print()

#def flatten1(lists):
 #   results1 =[]
 #   for number in lists:
  #      for i in number:
  #          results1.append(i)
 #   return results1

#print (flatten1(newlist1))
#print (flatten1(newlist2))


#def flatten2(lists):
 #   results2 =[]
  #  for number in lists:
   #     for i in number:
    #        results2.append(i)
    #return results2

#print (flatten2(newlist2))
