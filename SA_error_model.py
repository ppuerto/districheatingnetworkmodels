from SALib.sample import saltelli
from error_model import evaluate_error

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from numpy import empty, pi, array, meshgrid

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Define the model inputs

problem = {
    'num_vars': 4,
    'names': ['nbr_mesh', 'diameter', 'length', 'flow_in'],
    'bounds': [[10, 100],
               [0.2, 0.5],
               [75, 125],
               [0.025, 0.075]]
}

# Generate samples
print('Generating samples ...')
param_values = saltelli.sample(problem, 1, calc_second_order=True)
for x in param_values: # Loop for:mesh -> integer number
    x[0] = int(round(x[0], 0))
print('Input samples: ({})'.format(len(param_values)))
print(param_values) # generate 100*(2.4 + 2) = 1000 samples = 1000 [. . . .], 4 being de nmbres of inputs

# Run model
print('Running model...')
Y = empty([param_values.shape[0]])

for i, X in enumerate(param_values):

    Y[i] = evaluate_error(X) # return vector, length 1000 of differences(res,ref)


# Compute characteristics
c_time = [] # Time characteristic
c_mesh = [] # Dimension characteristic

for sample in param_values.tolist():
    c_time.append((pi * sample[1] ** 2 / 4 * sample[2]) / sample[3])  # You can compute characteristic time here
    c_mesh.append(pi * sample[1] ** 2 / 4 * sample[2] / sample[0])  # You can compute mesh volume here

# Plot 3D :
ax.scatter(c_time, c_mesh, Y) # Plot of time characteristic = x, dimension characteristic = y
                            # , error between temperature ref case and our case = z
# Plot up and low plan for error +++++++++++++++++++++++++++++++++++++++
point_up = array([0, 0, 1.05])
point_low = array([0, 0, 0.95])
normal = array([0, 0, 1])

d_up = - point_up.dot(normal)
d_low = - point_low.dot(normal)


xx, yy = meshgrid(range(800), range(4)) # Representation of the surface on 4 points axe y and 800 points axe x

z_up = (-normal[0] * xx - normal[1] * yy - d_up) * 1. / normal[2]
z_low = (-normal[0] * xx - normal[1] * yy - d_low) * 1. / normal[2]

ax.plot_surface(xx, yy, z_up, color='r', alpha=0.1)
ax.plot_surface(xx, yy, z_low, color='r', alpha=0.1)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ax.set_xlabel('X Label - time constant [s]')
ax.set_ylabel('Y Label - mesh volume [m3]')
ax.set_zlabel('Z Label - error [%]')


plt.ylim([-1, 4])

plt.show()

