from numpy import linspace, pi
from scipy.integrate import odeint




T_ext = 50  # °C
T_in = 70  # °C
Time_step = 3600  # [s]
rho = 1000  # [kg/m3]
cp = 4186  # [J/kgK]
U = 1.5  # [W/m2/K] Overall heat transfer coeff


def evaluate_model(X):
    l = X[0]  # length pipe
    d = X[1]  # diameter pipe
    N = X[2]  # Number of meshes
    mp = X[3]  # Flow in

    V_mesh = pi * d ** 2 / 4 * l / N  # [m3] volume of the pipe for 1 mesh
    A_mesh = pi * d * l / N  # [m2] Area of the pipe for 1 mesh

    # meshes = [T_in] * N # on crée une liste

    def eq_diff(y, t):
        dydt = []

        for idx, t in enumerate([T_in] * N):  # J'ai pas compris ce que je dois écrire ici, c'est quoi enumerate ?
            if idx == 0:
                eq = (T_in - y[0]) * mp * rho * cp  # Energy Balance between meshes
                eq -= (y[0] - T_ext) * U * A_mesh  # Loss with environment
                eq /= V_mesh * rho * cp
                dydt.append(eq)  # Ici on rempli la list dydt

            else:
                eq = (y[idx - 1] - y[idx]) * mp * rho * cp  # Energy Balance between meshes
                eq -= (y[idx] - T_ext) * U * A_mesh  # Loss with environment
                eq /= V_mesh * rho * cp
                dydt.append(eq)

        return dydt

    time = linspace(0, Time_step, num=101)

    y0 = [T_in] * N

    sol = odeint(eq_diff, y0, time)

    T = sol[-1]
    T_out = T[-1]  # dernier element de la list

    return (T_out)


def evaluate_model_A(X): # Fonction used to return vector of Tau
    l_A = X[0]  # length pipe
    d_A = X[1]  # diameter pipe
    mp_A = X[3]  # Flow in

    Tau = mp_A / (l_A*d_A) # temps caractéristique


    return (Tau)

def evaluate_model_B(X): # # Fonction used to return vector of delta
    l_B = X[0]  # length pipe
    d_B = X[1]  # diameter pipe
    N_B = X[2]  # Number of meshes

    V_mesh_B = pi * d_B ** 2 / 4 * l_B / N_B  # [m3] volume of the pipe for 1 mesh

    delta = V_mesh_B # dimension caractéristique


    return (delta)

