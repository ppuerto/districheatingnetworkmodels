from numpy import linspace, pi
from scipy.integrate import odeint


# creation class Network
class Network:
    def __init__(self, arg_init):

        self.length = arg_init["length"]
        self.inner_diam = arg_init["inner_diam"]
        self.nbr_mesh = arg_init["nbr_mesh"]

        self.t_ext = 12  # [deg.C]
        self.u_tot = 1.5  # [W/m2/K]

        self.t_in = arg_init["t_init"]  # [deg.C]
        self.flow_in = 0.05  # [m3/s]
        self.t_out = arg_init["t_init"]  # [deg.C]

        self.fluid_velocity = round(self.flow_in / (pi * self.inner_diam ** 2 / 4), 2)

        self.mesh_volume = pi * self.inner_diam ** 2 / 4 * self.length / self.nbr_mesh
        self.mesh_loss_area = pi * self.inner_diam * self.length / self.nbr_mesh

        self.rho = 990
        self.cp = 4184

        self.meshes = [arg_init["t_init"]] * self.nbr_mesh

        self.attributes = ['t_ext', 't_in', 't_out', 'flow_in']

        self.time_step = 60  # [s]

    def create_eq_diff(self):
        def eq_diff(y, t):
            dydt = []
            for idx, t in enumerate(self.meshes):
                if idx == 0:
                    eq = (self.t_in - y[0]) * self.flow_in * self.rho * self.cp  # exchange between meshes
                    eq -= (y[0] - self.t_ext) * self.u_tot * self.mesh_loss_area  # loss with environment
                    eq /= self.mesh_volume * self.rho * self.cp
                    dydt.append(eq)

                else:
                    eq = (y[idx - 1] - y[idx]) * self.flow_in * self.rho * self.cp  # exchange between meshes
                    eq -= (y[idx] - self.t_ext) * self.u_tot * self.mesh_loss_area  # loss with environment
                    eq /= self.mesh_volume * self.rho * self.cp
                    dydt.append(eq)
            return dydt

        return eq_diff

    def step(self):
        t = linspace(0, self.time_step, num=2)
        print(t)
        y0 = self.meshes

        sol = odeint(self.create_eq_diff(), y0, t).tolist()

        self.meshes = [round(temp, 2) for temp in sol[-1]]
        self.t_out = self.meshes[-1]

        self.fluid_velocity = round(self.flow_in / (pi * self.inner_diam ** 2 / 4), 2)


if __name__ == '__main__':
    arg = {'length': 100, 'inner_diam': 0.25, 'nbr_mesh': 10, 't_init': 40}

    n = Network(arg)
    n.t_in = 80
    n.time_step = 120

    print('length =', n.length)
    print('type =', type(n))

    n.step()
    print('t_out =', n.t_out)
